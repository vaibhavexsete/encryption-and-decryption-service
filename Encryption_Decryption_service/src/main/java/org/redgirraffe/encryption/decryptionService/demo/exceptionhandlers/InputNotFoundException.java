package org.redgirraffe.encryption.decryptionService.demo.exceptionhandlers;

public class InputNotFoundException extends RuntimeException {

	/**
	 * this Class used here for display the message if Data is null.   
	 * 
	 */
	private static final long serialVersionUID = 8206811187379190711L;
	
	private String errorMessage;
	
	
	public InputNotFoundException() {

	}


	public InputNotFoundException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}


	public String getErrorMessage() {
		return errorMessage;
	}


}
