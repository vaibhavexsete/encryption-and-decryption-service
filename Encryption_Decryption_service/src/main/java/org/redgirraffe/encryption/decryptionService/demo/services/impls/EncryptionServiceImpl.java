package org.redgirraffe.encryption.decryptionService.demo.services.impls;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.redgirraffe.encryption.decryptionService.demo.model.EncryptionRequest;
import org.redgirraffe.encryption.decryptionService.demo.model.responses.EncryptionResponse;
import org.redgirraffe.encryption.decryptionService.demo.services.EncryptionService;
import org.springframework.stereotype.Service;

@Service
public class EncryptionServiceImpl implements EncryptionService{
	
	
	private static SecretKeySpec secretKey ;
    private static byte[] key ;
    
    private static String encryptedString;
 
    public static String getEncryptedString() {
		return encryptedString;
	}
	
	public static void setEncryptedString(String encryptedString) {
		EncryptionServiceImpl.encryptedString = encryptedString;
	}

	@Override
	public EncryptionResponse getencryptionrequest(EncryptionRequest request) throws UnsupportedEncodingException {
		MessageDigest sha= null;
		try {
			key = request.getKey().getBytes("UTF-8");
			System.out.println(key.length);
			sha = MessageDigest.getInstance("SHA-1");
			key = sha.digest(key);
	    	key = Arrays.copyOf(key, 16); // use only 
	    	System.out.println("first 128 bit" +key.length);
	    	System.out.println(new String(key,"UTF-8"));
		    secretKey = new SecretKeySpec(key, "AES");
		    System.out.println("new Encrypted key:" +secretKey);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
	}catch (UnsupportedEncodingException e) {
		e.printStackTrace();
	}
	    
		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			 setEncryptedString(Base64.getEncoder().encodeToString(cipher.doFinal(request.getData().getBytes("UTF-8"))));
			 System.out.println("Message:" +encryptedString);
		
		} catch (Exception e) {
		
			System.out.println("Error while encrypting: "+e.toString());

		}
		return null;
	}
}