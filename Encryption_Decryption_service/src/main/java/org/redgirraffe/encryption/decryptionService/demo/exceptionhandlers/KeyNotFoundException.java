package org.redgirraffe.encryption.decryptionService.demo.exceptionhandlers;

public class KeyNotFoundException extends Exception {

	/**
	 * this Class used here for display the message if key is null.   
	 * 
	 */
	private static final long serialVersionUID = -357086321991762696L;
	
	private String errorMsg;
	
	public KeyNotFoundException() {

	}
	
	public KeyNotFoundException(String errorMsg) {
		super(errorMsg);
		this.errorMsg = errorMsg;
	}

	public String getErrormsg() {
		return errorMsg;
	}
}