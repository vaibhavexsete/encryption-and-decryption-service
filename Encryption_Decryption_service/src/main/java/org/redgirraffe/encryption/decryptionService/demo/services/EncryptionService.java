package org.redgirraffe.encryption.decryptionService.demo.services;

import org.redgirraffe.encryption.decryptionService.demo.model.EncryptionRequest;
import org.redgirraffe.encryption.decryptionService.demo.model.responses.EncryptionResponse;

public interface EncryptionService {

	public EncryptionResponse getencryptionrequest(EncryptionRequest request);
}
