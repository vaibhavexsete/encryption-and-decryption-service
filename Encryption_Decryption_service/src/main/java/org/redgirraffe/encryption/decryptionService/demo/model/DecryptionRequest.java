package org.redgirraffe.encryption.decryptionService.demo.model;

public class DecryptionRequest {
	/**
	 * this Class is used here for take request's parameters decryption of data.   
	 * 
	 */
	
	private String key;
	private String data;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "DecryptionRequest [key=" + key + ", data=" + data + "]";
	}
	
	
}
