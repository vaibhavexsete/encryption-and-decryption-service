package org.redgirraffe.encryption.decryptionService.demo.services;

import org.redgirraffe.encryption.decryptionService.demo.model.DecryptionRequest;
import org.redgirraffe.encryption.decryptionService.demo.model.responses.DecryptionResponse;

public interface DecryptionService {

	 public DecryptionResponse getdecryptionrequest(DecryptionRequest decryptionrequest);
}
