package org.redgirraffe.encryption.decryptionService.demo.model.responses;

import org.springframework.stereotype.Service;

@Service
public class DecryptionResponse {
	/**
	 * this Class is used here to provide the result based on operation(Encryption and decryption).   
	 * 
	 */
	
	private String decryptedMsg;
	
	public DecryptionResponse() {

	}

	public DecryptionResponse(String decryptedMsg) {
		
		this.decryptedMsg = decryptedMsg;
	}

	public String getdecryptedMsg() {
		return decryptedMsg;
	}

	public void setdecryptedMsg(String decryptedMsg) {
		this.decryptedMsg = decryptedMsg;
	}

	@Override
	public String toString() {
		return "DecryptionResponse [decryptedMsg=" + decryptedMsg + "]";
	}
	
	
	
}
