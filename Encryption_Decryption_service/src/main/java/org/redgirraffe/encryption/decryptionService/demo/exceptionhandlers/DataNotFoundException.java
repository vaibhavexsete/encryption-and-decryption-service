package org.redgirraffe.encryption.decryptionService.demo.exceptionhandlers;

public class DataNotFoundException extends RuntimeException {

	/**
	 * this Class used here for display the message if Data is null.   
	 * 
	 */
	private static final long serialVersionUID = 7034853643517652511L;
	
	private String errorMessage;
	
	public DataNotFoundException() {
	
	}

	public DataNotFoundException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}


}