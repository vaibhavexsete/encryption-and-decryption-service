package org.redgirraffe.encryption.decryptionService.demo.exceptionhandlers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@RestController
public class CustomizedExceptionHandler {
	
	/**
	 * this Class is acting as Global Exception handler class which is use to raise the exception.    
	 * 
	 */
	
	
	@ExceptionHandler(Exception.class)
	  public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		ExceptionDetails errorDetails = new ExceptionDetails( ex.getMessage(), request.getDescription(true));
	    return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	  }
	
	
	@ExceptionHandler(KeyNotFoundException.class)
	  public final ResponseEntity<Object> handleKeyNotFoundException(Exception ex, WebRequest request) {
		ExceptionDetails errorDetails = new ExceptionDetails( ex.getMessage(), request.getDescription(true));
	    
		//ExceptionDetails errordetails= new ExceptionDetails();
//		errordetails.setMessage("key not found!!");
//		errordetails.setDescription(request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	  }
	
	
	@ExceptionHandler(DataNotFoundException.class)
	public final ResponseEntity<Object> handleEncyptedMessageNotFound(Exception ex, WebRequest request){
		ExceptionDetails error = new ExceptionDetails(ex.getMessage(),request.getDescription(false));
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	
	@ExceptionHandler(InputNotFoundException.class)
	public final  ResponseEntity<Object> handleDataNotFoundException( Exception ex,WebRequest request){
		ExceptionDetails erroeDetails = new ExceptionDetails(ex.getMessage(),request.getDescription(false));
		return new ResponseEntity<> (erroeDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}