package org.redgirraffe.encryption.decryptionService.demo.exceptionhandlers;

public class ExceptionDetails {
	
	/**
	 * this Class is used to display the exception details.   
	 * 
	 */
	
	private String message;
	private String description;
	
	public ExceptionDetails() {
	
	}

	public ExceptionDetails(String message, String description) {
		super();
		this.message = message;
		this.description = description;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}