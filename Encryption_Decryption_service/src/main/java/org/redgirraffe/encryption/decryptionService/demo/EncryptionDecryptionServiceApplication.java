package org.redgirraffe.encryption.decryptionService.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EncryptionDecryptionServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EncryptionDecryptionServiceApplication.class, args);
	}
}