package org.redgirraffe.encryption.decryptionService.demo.controllers;

import org.redgirraffe.encryption.decryptionService.demo.exceptionhandlers.DataNotFoundException;
import org.redgirraffe.encryption.decryptionService.demo.exceptionhandlers.KeyNotFoundException;
import org.redgirraffe.encryption.decryptionService.demo.model.DecryptionRequest;
import org.redgirraffe.encryption.decryptionService.demo.model.responses.DecryptionResponse;
import org.redgirraffe.encryption.decryptionService.demo.services.DecryptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DecryptionController {
	
	
	/**
	 * this Class is used here for take requests and process them for Decryption.   
	 * 
	 */
	
	
	@Autowired
	DecryptionService decryptionservice;
	
	
//	@RequestMapping(value ="/decrypt",method = RequestMethod.POST)
	@RequestMapping(value ="/decrypt",method = RequestMethod.GET)
	public 	DecryptionResponse  doDecryption(@RequestBody DecryptionRequest decryptionrequest) throws KeyNotFoundException {
		System.out.println("Request coming for decryption:");

		if(decryptionrequest!=null) {
			if(decryptionrequest.getKey()==null) {
					throw new KeyNotFoundException();
			}else if (decryptionrequest.getData()==null) {
				throw new DataNotFoundException();
			}
		}
		return decryptionservice.getdecryptionrequest(decryptionrequest);
	}
}