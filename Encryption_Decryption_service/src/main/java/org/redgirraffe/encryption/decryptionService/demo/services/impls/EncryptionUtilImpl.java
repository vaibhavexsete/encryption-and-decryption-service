package org.redgirraffe.encryption.decryptionService.demo.services.impls;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Hex;
import org.redgirraffe.encryption.decryptionService.demo.model.DecryptionRequest;
import org.redgirraffe.encryption.decryptionService.demo.model.EncryptionRequest;
import org.redgirraffe.encryption.decryptionService.demo.model.responses.DecryptionResponse;
import org.redgirraffe.encryption.decryptionService.demo.model.responses.EncryptionResponse;
import org.redgirraffe.encryption.decryptionService.demo.services.DecryptionService;
import org.redgirraffe.encryption.decryptionService.demo.services.EncryptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EncryptionUtilImpl implements EncryptionService,DecryptionService{
	
	/**
	 * this Class is used here for taking parameters and producing the encrypt and decrypt data.   
	 * 
	 */
		
	@Autowired
	private EncryptionResponse encryptionResponse;

	@Autowired
	private DecryptionResponse decryptionResponse;

	@Override
	public EncryptionResponse getencryptionrequest(EncryptionRequest request) {
		try {
			byte[] res = Hex.decodeHex(request.getKey().toCharArray());
			PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(res));
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.PUBLIC_KEY, publicKey);
			encryptionResponse.setencryptedMsg(Base64.getEncoder().encodeToString(cipher.doFinal(request.getInput().getBytes("UTF-8"))));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return encryptionResponse;

	}

	@Override
	public DecryptionResponse getdecryptionrequest(DecryptionRequest decryptionrequest) {
		try {
			System.out.println("Request coming in this method!!");
			byte[] privateKeyBytes = Hex.decodeHex(decryptionrequest.getKey().toCharArray());
			PrivateKey privateKey = KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(privateKeyBytes));
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			decryptionResponse.setdecryptedMsg(new String(cipher.doFinal(Base64.getDecoder().decode(decryptionrequest.getData())), "UTF-8"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return decryptionResponse;
	}
}