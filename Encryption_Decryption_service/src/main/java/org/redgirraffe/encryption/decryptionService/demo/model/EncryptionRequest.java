package org.redgirraffe.encryption.decryptionService.demo.model;

public class EncryptionRequest {
	/**
	 * this Class is used here for take request's parameters for encryption of data.   
	 * 
	 */
	
	private String key;
	private String input;
	
	
	public EncryptionRequest() {

	}
	
	public EncryptionRequest(String key, String input) {
		super();
		this.key = key;
		this.input = input;
	}
	
	public String getKey() {
		return key;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	
	public String getInput() {
		return input;
	}
	
	public void setInput(String input) {
		this.input = input;
	}

	@Override
	public String toString() {
		return "EncryptionRequest [key=" + key + ", input=" + input + "]";
	}
	
	
}