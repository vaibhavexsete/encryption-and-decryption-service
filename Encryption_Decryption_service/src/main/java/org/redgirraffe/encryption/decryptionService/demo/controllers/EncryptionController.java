package org.redgirraffe.encryption.decryptionService.demo.controllers;

import java.io.UnsupportedEncodingException;

import org.redgirraffe.encryption.decryptionService.demo.exceptionhandlers.InputNotFoundException;
import org.redgirraffe.encryption.decryptionService.demo.exceptionhandlers.KeyNotFoundException;
import org.redgirraffe.encryption.decryptionService.demo.model.EncryptionRequest;
import org.redgirraffe.encryption.decryptionService.demo.model.responses.EncryptionResponse;
import org.redgirraffe.encryption.decryptionService.demo.services.EncryptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EncryptionController {
	
	/**
	 * this Class is used here for take requests and process them for Encryption.   
	 * 
	 */
	
	 
	@Autowired
	EncryptionService encryptionService;

	@RequestMapping(value = "/encrypt",method = RequestMethod.POST)
	public EncryptionResponse  doEncryption(@RequestBody EncryptionRequest request) throws KeyNotFoundException, UnsupportedEncodingException {
		System.out.println("Request coming for encryption:");
		if (request!=null) {
		if (request.getInput()==null) {
				throw new InputNotFoundException();
			} else 	if(request.getKey()==null) {
				throw new KeyNotFoundException();
			} 
		}
		return encryptionService.getencryptionrequest(request);
	}
}