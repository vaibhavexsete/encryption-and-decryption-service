package org.redgirraffe.encryption.decryptionService.demo.model.responses;

import org.springframework.stereotype.Service;

@Service
public class EncryptionResponse {
	
	/**
	 * this Class is used here to provide the result based on operation(Encryption and decryption).   
	 * 
	 */
	
	private String encryptedMsg;
	
	public EncryptionResponse() {

	}

	public EncryptionResponse(String encryptedMsg) {

		this.encryptedMsg = encryptedMsg;
	}

	public String getencryptedMsg() {
		return encryptedMsg;
	}
	
	
	

	@Override
	public String toString() {
		return "EncryptionResponse [encryptedMsg=" + encryptedMsg + "]";
	}

	public void setencryptedMsg(String encryptedMsg) {
		this.encryptedMsg = encryptedMsg;
	}
}
